FROM debian:buster-slim as odbc_driver
RUN apt-get update \
    && apt-get dist-upgrade -y \
    && apt-get install -y --no-install-recommends \
    alien wget ca-certificates\
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
# download dremio odbc rpm and convert to deb
RUN wget https://download.dremio.com/odbc-driver/1.5.1.1001/dremio-odbc-1.5.1.1001-1.x86_64.rpm \
    && alien dremio-odbc-1.5.1.1001-1.x86_64.rpm

FROM apache/superset:1.0.1

USER root
RUN apt-get update \
    && apt-get dist-upgrade -y \
    && apt-get install -y --no-install-recommends \
    unixodbc unixodbc-dev
# odbc setup
COPY --from=odbc_driver dremio-odbc_1.5.1.1001-2_amd64.deb .
RUN apt-get update \
    && apt-get install ./dremio-odbc_1.5.1.1001-2_amd64.deb  \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* dremio-odbc_1.5.1.1001-2_amd64.deb

# install dbt-dremio
RUN pip install pyodbc sqlalchemy_dremio

COPY ./config/ /etc/
USER superset
