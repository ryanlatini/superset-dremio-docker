# Docker Image for Superset 

A Docker image for [Apache Superset](https://github.com/apache/superset) configured for use with Dremio.

## License

MIT License Copyright (c) 2021 Ryan Latini